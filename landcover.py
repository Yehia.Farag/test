import numpy as np
import matplotlib.pyplot as plt
### task 1 ###
input_data = "data/gl-latlong-1km-landcover.bsq"
dataset_file = np.fromfile(input_data, dtype='uint8')
dataset_file = dataset_file.reshape(21600, 43200)
# print(dataset_file)
data_to_show = dataset_file[::50, ::50]
plt.imshow(data_to_show)
# plt.show()
max_x_pix = 43201.
max_y_pix = 21601.

landscape = {0: "Water", 1: "Evergreen Needleleaf forest", 2: "Evergreen Broadleaf Forest", 3: "Deciduous Needleleaf Forest", 4: "Dec Bro Forest", 5: "Mixed Forest",
             6: "Woodland", 7: "Wooded grassland", 8: "closed shrubland", 9: "open shrubland", 10: "Grassland", 11: "Cropland", 12: "Bare ground", 13: "Urban and built"}


def coords_to_pix(coor_y, coor_x):
    coor_x = coor_x+180.
    converted_x = coor_x * max_x_pix/360.
    coor_y = abs(coor_y - 90.)
    converted_y = coor_y * max_y_pix/180.
    return (converted_y, converted_x)


def nature_type(coor_y, coor_x):
    if coor_x >= -180 and coor_x <= 180 and coor_y >= -90 and coor_y <= 90:
        updated_coor_pix = coords_to_pix(coor_y, coor_x)
        landscape.get(
            dataset_file[int(updated_coor_pix[0]), int(updated_coor_pix[1])])
        return landscape.get(dataset_file[int(updated_coor_pix[0]), int(updated_coor_pix[1])])
    else:
        return "out of scope"


print(nature_type(49.95, 9.13))
print("Pattama")

### task 2 ###
##earthquake_data = np.fromfile("data/events_4.5.txt")

